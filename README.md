# README #


### Introduction ###
In this project, we explore several techniques for constructing a dataset of fake reviews.
The constructed dataset is then used to build models that can classify a review as
fake.


### Techniques Used ###

* Document TF/IDF
* Association class rules
* Language Modeling
* Naive-Bayes classification 

### Results ###

* F1 Score for TruthFul opinion model: 80%
* F1 Score for Deceptive opinion model: 75%
